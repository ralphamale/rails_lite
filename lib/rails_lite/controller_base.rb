require 'erb'
require 'active_support/inflector'
require_relative 'params'
require_relative 'session'
require 'debugger'
require 'active_support/core_ext'

class ControllerBase
  attr_reader :params, :req, :res

  # setup the controller
  def initialize(req, res, route_params = {})
    @req = req
    @session = Session.new(@req)
    @params = Params.new(@req, route_params)
    @res = res

    @res.body = @req.body

    @already_built_response = false
    #@res.cookies = @req.cookies
    #query strings
    # it will use the request (its query string, cookies, body content) to help fill out the response.

  end

  # populate the response with content
  # set the responses content type to the given type
  # later raise an error if the developer tries to double render
  def render_content(content, type)

    @res.body = content
    @res.content_type = type
    raise "Already rendered" if already_rendered?
    @session.store_session(@res)
    @already_built_response = true


  end

  # helper method to alias @already_rendered
  def already_rendered?
    @already_built_response
  end

  # set the response status code and header
  def redirect_to(url)

    @res.status = 302
    @res['location'] = url
    raise "Already rendered" if already_rendered?
    @session.store_session(@res)
    @already_built_response = true
  end



  # use ERB and binding to evaluate templates
  # pass the rendered html to render_content
  def render(template_name)

    mode = "rb"
    contents = File.open("views/#{underscore(self.class)}/#{template_name}.html.erb", mode) { |f| f.read }


    template = ERB.new(contents).result(binding)
    render_content(template, "text/html")




    # users_controller.res.body.should include("users")
    # users_controller.res.body.should include("<h1>")
    # users_controller.res.content_type.should == "text/html"

  end

  def underscore(camel_cased_word)
    camel_cased_word.to_s.gsub(/::/, '/').
      gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
      gsub(/([a-z\d])([A-Z])/,'\1_\2').
      tr("-", "_").
      downcase
  end

  # method exposing a `Session` object
  def session
    @session
  end

  # use this with the router to call action_name (:index, :show, :create...)
  def invoke_action(name)
    self.send(name)
  end

  # def render_content(content, type)
  # end
end

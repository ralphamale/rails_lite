require 'uri'

class Params
  # use your initialize to merge params from
  # 1. query string
  # 2. post body
  # 3. route params
  def initialize(req, route_params = {})
    @params = route_params

    parse_www_encoded_form(req.query_string)
    parse_www_encoded_form(req.body)
  end

  def [](key)
    @params[key]
  end

  def to_s
    @params.to_s
  end

  private
  # this should return deeply nested hash
  # argument formatuser[address][street]=main&user[address][zip]=89436
  #
  # should return
  # { "user" => { "address" => { "street" => "main", "zip" => "89436" } } }
  def parse_www_encoded_form(www_encoded_form)
    return if www_encoded_form.nil?

    puts "www_encoded_form"
    puts www_encoded_form

    params_array = URI.decode_www_form(www_encoded_form, enc=Encoding::UTF_8)
    puts "params array"
    puts params_array.to_s

    params_array.each do |key,value|
      parsed_keys = parse_key(key)
      current_level_hash = @params

      until parsed_keys.count == 1
        current_key = parsed_keys.shift
        if current_level_hash.has_key?(current_key)
          current_level_hash = current_level_hash[current_key]
        else
          current_level_hash[current_key] = {}
          current_level_hash = current_level_hash[current_key]
        end
      end

      current_level_hash[parsed_keys.shift] = value
    end


  end


  # this should return an array
  # user[address][street] should return ['user', 'address', 'street']
  def parse_key(key)
    key.split(/\]\[|\[|\]/)
  end
end

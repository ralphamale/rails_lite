require 'json'
require 'webrick'

class Session
  # find the cookie for this app
  # deserialize the cookie into a hash
  def initialize(req)
    req_cookies = req.cookies

    cookies_found = req_cookies.select { |cookie| cookie.name == '_rails_lite_app' }
    if cookies_found.count == 0
      @session = {}
    else
      cookies = cookies_found.first
      @session = JSON.parse(cookies.value)
    end


  end

  def [](key)
    @session[key]
  end

  def []=(key, val)
    @session[key] = val
  end

  #
  # serialize the hash into json and save in a cookie
  # add to the responses cookies
  def store_session(res)
    res.cookies.push WEBrick::Cookie.new('_rails_lite_app', @session.to_json)
  end
end
